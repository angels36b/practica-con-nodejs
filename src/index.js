const express = require('express')
const path = require('path')
const connection = require('./connection')
const app = express()

const user = require('./routes/users')
const loggedMiddleware = require('./middlewares/logged')
    //setting
app.set('title', ' aplicacion en Node')
app.set('port', 3000)
app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, 'views'))
    //middlewares
    //app.use(loggedMiddleware.isLogged)
app.use(express.static(path.join(__dirname, 'public')))
app.use(express.urlencoded({ extended: false }))
    //rutas
app.get('/', (req, res) => {
    res.render('index')
})

app.use(user)

//habilito mi servidor para que escuche por un puerto
app.listen(app.get('port'), () => {
    console.log('Mi' + app.get('title') + ' esta corriendo Puerto ' + app.get('port'))
})