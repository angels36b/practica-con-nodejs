const mysql = require('mysql')
const { mysql_database } = require('./config')
const connection = mysql.createConnection(mysql_database)

connection.connect((err, conn) => {

    if (err) {
        console.log('ha ocurrido un error al conectarse')
    } else {
        console.log('conexion exitosa 200')
        return conn
    }
})

module.exports = connection